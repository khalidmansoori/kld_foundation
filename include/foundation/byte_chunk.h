// Copyright (C) 2018 kld foundation library authors. All rights reserved.
//
// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#pragma once

#include <string>
/*
 * Provides a simple vanila structure to hold a byte stream of known length
 * helps with dealing with binary data received over netwrok
 */
namespace kld
{
namespace foundation
{
struct byte_chunk {
    const uint8_t* chunk;
    size_t size;

    byte_chunk(const uint8_t* ptr, size_t chunkSize) : chunk(ptr), size(chunkSize) {}
    byte_chunk() : chunk(nullptr), size(0) {}

    bool empty()
    {
        if (chunk == nullptr || size == 0) {
            return true;
        }
        return false;
    }

    // moves chunks by n number of bytes.
    // resets to null/zero if size is more then number of bytes
    void move(size_t num_bytes)
    {
        if (num_bytes > size) {
            chunk = nullptr;
            size = 0;
            return;
        }

        chunk += num_bytes;
        size -= num_bytes;
    }

    friend std::ostream& operator<<(std::ostream& os, const byte_chunk& bytes)
    {
        if (bytes.chunk == nullptr || bytes.size == 0) {
            os << "size = 0, chunk = null";
            return os;
        }

        std::string chunkStr((char*)bytes.chunk, bytes.size);
        os << "size = " << bytes.size << ", chunk = (" << chunkStr << ")";
        return os;
    }
};
} 
}
