// Copyright (C) 2018 kld foundation library authors. All rights reserved.
//
// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed 
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
// CONDITIONS OF ANY KIND, either express or implied. See the License for the 
// specific language governing permissions and limitations under the License.

#pragma once

#include <algorithm>
#include <functional>
#include <memory>
#include <vector>

namespace kld {
namespace foundation {
template <typename T> class observer_t {
public:
  virtual void on_update(T) = 0;
};

// We don't want to impose on_update for every observers
// they are free to call any functions by passing
// function argument in notify_observers. hence don't use
// observer_t as type of observers.

template <typename Observer_> class observable_t {
public:
  void add_observer(const std::weak_ptr<Observer_> &observer) {
    if (observer.expired()) {
      return;
    };

    const auto& itr = std::find_if(observers_.begin(), observers_.end(),
                            [&](const std::weak_ptr<Observer_> &o) {
                              return o.lock() == observer.lock();
                            });

    // not found, this is new observer.
    if (itr == observers_.end()) {
      observers_.push_back(observer);
    }
  }

  void remove_observer(const std::weak_ptr<Observer_> &observer) {
    // There is no way to find the associated observer from the expired weak
    // pointer
    if (observer.expired()) {
      return;
    }

    // Remove the given observer and also use this chance to clean the dead
    // observers.
    observers_.erase(std::remove_if(observers_.begin(), observers_.end(),
                                    [&](const std::weak_ptr<Observer_> &o) {
                                      return o.expired() ||
                                             o.lock() == observer.lock();
                                    }),
                     observers_.end());
  }

  // Notifies all the active observers and calls the passed function on each observers.
  // Able to accept functions with variable length arguments.
  // Inspired by std::thread.
  template <typename Callable_, typename... Args_>
  void notify_observers(Callable_ &&func, Args_ &&... args) {
    notify_internal(std::forward<std::function<void(std::shared_ptr<Observer_>)>>(
        std::bind(func, std::placeholders::_1, args...)));
  }

  // Updates observers via on_update call.
  // Special cases for value observations.
  // TO-DO Add type check for observer type only with concepts, however we will get compile
  // time error if different type is used.
  template <typename T> void update_observers(T data) {
    notify_observers(&Observer_::on_update, data);
  }

protected:
  void notify_internal(std::function<void(std::shared_ptr<Observer_>)> func) {
    auto weak_observer_itr = observers_.begin();
    // safe iteration while removing dead observers
    while (weak_observer_itr != observers_.end()) {
      auto observer = weak_observer_itr->lock();
      if (observer == nullptr) {
         weak_observer_itr = observers_.erase(weak_observer_itr);
        continue;
      }
      ++weak_observer_itr;
      execute_internal(
          std::forward<std::function<void(void)>>(std::bind(func, observer)));
    }
  }

  // NOT required, kept for future work for storing executable functions
  // and execute all at once.
  void execute_internal(const std::function<void(void)> &func) {
    func();
  }

protected:
  std::vector<std::weak_ptr<Observer_>> observers_;
};

} // namespace foundation
} // namespace kld