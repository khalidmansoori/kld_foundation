#pragma once
#include <algorithm>
#include <iostream>
#include <vector>
#include "foundation/byte_buffer.h"
#include "rapidjson/document.h"
#include "rapidjson/reader.h"

namespace kld::foundation
{
class IJSONStreamParserDelegate
{
   public:
    virtual bool Process(const std::shared_ptr<rapidjson::Document> json) = 0;
};

template <size_t max_chunk_size_>
class JSONByteStreamParser
{
   public:
    // Parses the current stream and returns and calls Process on delegate
    //  keep the left over data to be parsed in next iterations.
    JSONByteStreamParser(std::weak_ptr<IJSONStreamParserDelegate> delegate)
        : delegate_(delegate), numberOfJSONParsed_(0)
    {
    }

    bool ParseStream(const uint8_t* chunk, size_t chunkSize)
    {
        kld::foundation::byte_chunk jsonChunk(chunk, chunkSize);
        return ParseStream(jsonChunk);
    }

    bool ParseStream(kld::foundation::byte_chunk bytes) { return ParseJSONChunkInternal(bytes); }

    const char* Data() { return byte_buffer_.c_str(); }

    size_t NumberOfJSONParsed() { return numberOfJSONParsed_; }

    void Reset()
    {
        byte_buffer_.clear();
        numberOfJSONParsed_ = 0;
    }

   private:
    size_t ParseJSONByteBuffer(std::shared_ptr<rapidjson::Document> json)
    {
        rapidjson::StringStream jsonStream(byte_buffer_.c_str());
        size_t failed_json_offset = 0;
        do {
            json->ParseStream<rapidjson::kParseStopWhenDoneFlag>(jsonStream);
            if (json->HasParseError()) {
                break;
            }

            if (auto jsonProcesssor = delegate_.lock()) {
                numberOfJSONParsed_++;
                jsonProcesssor->Process(json);
            }

            failed_json_offset = jsonStream.Tell();
        } while (json->HasParseError() == false && jsonStream.Tell() != byte_buffer_.size());
        return failed_json_offset;
    }

    bool ParseJSONChunkInternal(kld::foundation::byte_chunk jsonChunk)
    {
        kld::foundation::byte_chunk overflow = jsonChunk;
        auto json = std::make_shared<rapidjson::Document>();

        while (overflow.chunk != nullptr) {
            overflow = byte_buffer_.insert_back(overflow);

            size_t failed_json_offset = ParseJSONByteBuffer(json);
            if (json->HasParseError() == false) {
                // No Error, erase the data and get ready for next.
                byte_buffer_.clear();
            } else {
                // The successfully parsed json till offset value.
                byte_buffer_.remove_front(failed_json_offset);
            }

            if (byte_buffer_.is_full() && json->HasParseError()) {
                // Check if overflow is never null ?
                // Ideally speaking we should be able to fit everything in buffer in
                // first place, however in a rainy day when there is courrpted json
                // means nothing is getting parsed.
                // we can decide , if buffer is full and still there is parser failure
                // stop processing. and report error.
                return false;
            }
        }

        return true;
    }
    // disable assignment copy and move.
    JSONByteStreamParser(const JSONByteStreamParser&) = delete;
    JSONByteStreamParser(const JSONByteStreamParser&&) = delete;
    JSONByteStreamParser(const JSONByteStreamParser*) = delete;
    JSONByteStreamParser& operator=(const JSONByteStreamParser&) = delete;

   private:
    kld::foundation::byte_buffer<max_chunk_size_> byte_buffer_;
    std::weak_ptr<IJSONStreamParserDelegate> delegate_;
    size_t numberOfJSONParsed_;
};
}  // namespace kld::foundation