// Copyright (C) 2018 kld foundation library authors. All rights reserved.
//
// Licensed under the MIT License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// http://opensource.org/licenses/MIT
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

#pragma once

/*
 * This class provides an stl std::array like functionality to deal with continuous stream of bytes
 * received over network. The user can keep inserting the data until it is ready to be processed, if 
 * data can not fit into capacity the remaining data is returned back to be used for next iteration of processing.
 * This help the use of class to avoid managing the buffer and its memory management.
 * It efficiently reuses the same allocated memory for continuous stream with reallocation on erase.
 * Moreover, it does NOT deletes the memory on erase it simply manipulates the pointers.
 *
 */

#include <assert.h>
#include <cstdint>
#include <cstring>  //memcpy
#include <memory>
#include <string>
#include <vector>
#include "byte_chunk.h"

namespace kld {
namespace foundation
{
// template parameter allows us to have buffer on stack.
// earlier it was decided to have stack for speed, however
// to avoid overflow that may occur for very large size
// heap was choosen.
template <size_t buffer_capacity_>
class byte_buffer {
   public:
    byte_buffer()
    {
        buffer_ = new uint8_t[buffer_capacity_ + 1];
        clear();
    }

    ~byte_buffer() { delete[] buffer_; }
    size_t capacity() { return buffer_capacity_; }
    size_t size() { return current_buffer_size_; }
    bool is_empty() { return (current_buffer_size_ == 0); }
    bool is_full() { return (current_buffer_size_ == buffer_capacity_); }

    void clear()
    {
        memset(buffer_, 0, buffer_capacity_ + 1);
        current_position_ = buffer_;
        current_buffer_size_ = 0;
    }

    /*
     * Returns a c string pointer to internal buffer.
     */
    const char* c_str() { return reinterpret_cast<const char*>(current_position_); }
    
    /*
     * Returns pointer to internal buffer.
     */
    const uint8_t* data() { return current_position_; }

    /*
     * Returns current data as byte_chunk to internal buffer.
     */
    const byte_chunk chunk()
    {
        return byte_chunk(current_position_, current_buffer_size_);
    }

    // size_t insert_front(const char *ptr, size_t size) {
    // 	//TO-DO
    // 	assert(false);
    // }

    /*
     * Inserts the given chunk into buffer, the overflow is return back to be used in next iterations.
     */
    byte_chunk insert_back(const byte_chunk& bytes)
    {
        size_t overflow = insert_back(bytes.chunk, bytes.size);
        if (overflow > 0) {
            uint8_t new_position = bytes.size - overflow;
            byte_chunk overflow = bytes;
            overflow.move(new_position);
            return overflow;
        }

        return byte_chunk();
    }

    /*
     * Inserts the given data of size into the buffer.
     * @returns the postion in data which was't fit into the buffer.
     */
    size_t insert_back(const uint8_t* ptr, size_t size)
    {
        if (can_fit(size)) {
            insert_internal(ptr, size);
            return 0;
        }

        size_t remaining_capacity = buffer_capacity_ - current_buffer_size_;
        insert_internal(ptr, remaining_capacity);
        return size - remaining_capacity;
    }

    /* 
     * If given character is found remove data from begin to character
     * position, else nothin is removed.
     */
    bool remove_front_till(uint8_t ch)
    {
        if (current_position_ == 0) {
            return false;
        }

        size_t location = 0;
        for (; location <= current_buffer_size_; ++location) {
            if (*(current_position_ + location) == ch) {
                location++;
                break;
            }
        }

        if (location == 0) {
            return false;
        }

        remove_front(location);
        return true;
    }

    /*
     * Removed the given number of bytes from the front, if size is more than size of buffer
     * simply cleans the buffer.
     */
    void remove_front(size_t size)
    {
        // clear everything if cleaning more than what is already there.
        // simple instead of assertoion.
        if (size >= current_buffer_size_) {
            clear();
            return;
        }

        // memset can be avoided, lets see if there is performace
        // issue for very large data.
        memset(current_position_, 0, size);
        current_position_ += size;
        current_buffer_size_ -= size;
    }

    // size_t remove_back(size_t size) {
    // TO-DO
    // assert(false);
    // }

   private:

    /*
     * Check if give size of data can be fit into buffer.
     */
    bool can_fit(size_t size) { return (current_buffer_size_ + size <= buffer_capacity_); }

    /*
     * If data can be fit into the buffer however it can be not appended
     * because of internal buffers postion is not at start.
     * aligns data to begin of buffer to accomodate the new data.
     */

    void correct_positions_to_fit(size_t size)
    {
        // Since we are not removing when remove is called
        // just a buffer moevment is done.
        // if this is new data, position it back to begin.
        assert(can_fit(size));
        uint8_t* target_position = current_position_ + current_buffer_size_ + size;
        if (target_position > buffer_ + buffer_capacity_) {
            memcpy(buffer_, current_position_, current_buffer_size_);
            current_position_ = buffer_;
        }
    }

    // does the real insert of data into buffer

    void insert_internal(const uint8_t* ptr, size_t size)
    {
        assert(can_fit(size));
        correct_positions_to_fit(size);
        memcpy(current_position_ + current_buffer_size_, ptr, size);
        current_buffer_size_ += size;
    }

    // delete assignment, copy, move until we overload operator and do deep copy
    byte_buffer(const byte_buffer&) = delete;
    byte_buffer(const byte_buffer&&) = delete;
    byte_buffer(const byte_buffer*) = delete;
    byte_buffer& operator=(const byte_buffer&) = delete;

   private:
    uint8_t* buffer_;            // Internal buffer to hold the given data
    uint8_t* current_position_;  // current positon of begining of real data in buffer
    size_t current_buffer_size_; // size of data in buffer
};
}
}
