# compile 
sample code from terminal as follow

```
g++ sample/ObserverSample.cpp --std=c++11 -I include/

```

# compile and run for gtest

```
g++ test/ObserverTest.cpp --std=c++11 -D USE_GTEST_HEADER_ONLY -D USE_GTEST_MAIN -I ./include -I thirdparty/googletest-release-1.8.0/googletest/include/ -I thirdparty/googletest-release-1.8.0/googletest/ -lpthread

g++ test/ByteBufferTest.cpp --std=c++11 -D USE_GTEST_HEADER_ONLY -D USE_GTEST_MAIN -I ./include -I thirdparty/googletest-release-1.8.0/googletest/include/ -I thirdparty/googletest-release-1.8.0/googletest/ -lpthread

g++ test/JSONStreamParserTest.cpp --std=c++11 -D USE_GTEST_HEADER_ONLY -D USE_GTEST_MAIN -I ./include -I thirdparty/googletest-release-1.8.0/googletest/include/ -I thirdparty/googletest-release-1.8.0/googlemock/include/ -I thirdparty/googletest-release-1.8.0/googletest/ -lpthread

```
