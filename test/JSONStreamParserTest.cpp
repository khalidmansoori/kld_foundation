#ifdef USE_GTEST_HEADER_ONLY
#include "src/gtest-all.cc"
#undef USE_GTEST_HEADER_ONLY
#endif

#ifdef USE_GTEST_MAIN
#include "src/gtest_main.cc"
#undef USE_GTEST_MAIN
#endif

#ifdef USE_GMOCK_MAIN
#include "src/gmock_main.cc"
#undef USE_GMOCK_MAIN
#endif

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <string.h>
#include <vector>
#include "foundation/json/JSONStreamParser.h"

using ::testing::_;
using namespace kld::foundation;
class MOCJSONStreamParserDelegate : public IJSONStreamParserDelegate {
   public:
    MOCK_METHOD1(Process, bool(const std::shared_ptr<rapidjson::Document> json));
};

class JSONStreamProcessor : public IJSONStreamParserDelegate
{
   public:
     bool Process(const std::shared_ptr<rapidjson::Document> json){
         jobQueue_.push_back(json);
         return true;
     }

     auto GetJobs() { return jobQueue_; }

    private:
     std::vector<std::shared_ptr<rapidjson::Document> > jobQueue_;
};

TEST(TestSuiteCommonUtil, TestJSONStreamParser)
{
    const char json_string[] =
        "{ \"hello1\" : \"world1\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": "
        "3.1416, \"a\":[1, 2, 3, 4] } "
        "{ \"hello2\" : \"world2\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": "
        "3.1416, \"a\":[1, 2, 3, 4] } "
        "{ \"hello3\" : \"world3\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": "
        "3.1416, \"a\":[1, 2, 3, 4] } "
        "{ \"hello4\" : \"world4\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": "
        "3.1416";

    const char next_json_string[] = ", \"a\":[1, 2, 3, 4] }";
    std::shared_ptr<MOCJSONStreamParserDelegate> mockProcessor =
        std::make_shared<MOCJSONStreamParserDelegate>();
    JSONByteStreamParser<110> jsonParser(mockProcessor);

    EXPECT_CALL(*mockProcessor.get(), Process(_)).Times(3);
    EXPECT_TRUE(jsonParser.ParseStream((uint8_t*)json_string, strlen((char*)json_string)));

    EXPECT_STREQ(jsonParser.Data(),
                 " { \"hello4\" : \"world4\", \"t\" : true , \"f\" : false, \"n\": null, "
                 "\"i\":123, \"pi\": 3.1416");
    EXPECT_TRUE(jsonParser.NumberOfJSONParsed() == 3);

    EXPECT_CALL(*mockProcessor.get(), Process(_)).Times(1);
    EXPECT_TRUE(
        jsonParser.ParseStream((uint8_t*)next_json_string, strlen((char*)next_json_string)));
    EXPECT_STREQ(jsonParser.Data(), "");
    EXPECT_TRUE(jsonParser.NumberOfJSONParsed() == 4);
}

TEST(TestSuiteCommonUtil, TestJSONStreamParserResetAfterMalformedJSON)
{
    const char error_json_string[] =
        "{ \"hello1\" : \"world1\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": "
        "3.1416, \"a\":[1, 2, 3, 4]  "
        " \"hello2\" : \"world2\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": "
        "3.1416, \"a\":[1, 2, 3, 4]  "
        " \"hello3\" : \"world3\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": "
        "3.1416, \"a\":[1, 2, 3, 4]  "
        " \"hello4\" : \"world4\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": "
        "3.1416";

    std::shared_ptr<MOCJSONStreamParserDelegate> mockProcessor =
        std::make_shared<MOCJSONStreamParserDelegate>();
    JSONByteStreamParser<110> jsonParser(mockProcessor);

    EXPECT_CALL(*mockProcessor.get(), Process(_)).Times(0);
    EXPECT_FALSE(
        jsonParser.ParseStream((uint8_t*)error_json_string, strlen((char*)error_json_string)));
    EXPECT_STREQ(jsonParser.Data(),
                 "{ \"hello1\" : \"world1\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, "
                 "\"pi\": 3.1416, \"a\":[1, 2, 3, 4]   \"hello2\"");
    EXPECT_TRUE(jsonParser.NumberOfJSONParsed() == 0);

    jsonParser.Reset();

    EXPECT_CALL(*mockProcessor.get(), Process(_)).Times(1);
    const char json_string[] =
        "{ \"hello1\" : \"world1\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": "
        "3.1416, \"a\":[1, 2, 3, 4] } ";
    EXPECT_TRUE(jsonParser.ParseStream((uint8_t*)json_string, strlen((char*)json_string)));
    // EXPECT_STREQ(jsonParser.Data(),"");
    EXPECT_TRUE(jsonParser.NumberOfJSONParsed() == 1);
}

TEST(TestSuiteCommonUtil, TestJSONStreamParserDockerEvent)
{
    const char json_string[] =
        "{\"status\":\"exec_create: sh -c export http_proxy=http://10.115.27.54:3128 \u0026\u0026 "
        "export https_proxy=http://10.115.27.54:3128 \u0026\u0026 apt-get update \u0026\u0026 "
        "apt-get -y upgrade \u0026\u0026 echo Done "
        "Httpddocker\",\"id\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"from\":\"httpd\","
        "\"Type\":\"container\",\"Action\":\"exec_create: sh -c export "
        "http_proxy=http://10.115.27.54:3128 \u0026\u0026 export "
        "https_proxy=http://10.115.27.54:3128 \u0026\u0026 apt-get update \u0026\u0026 apt-get -y "
        "upgrade \u0026\u0026 echo Done "
        "Httpddocker\",\"Actor\":{\"ID\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"Attributes\":{"
        "\"execID\":\"fab2b57669cf8e6d6184f030123325c709ad053e5230e35152afad6f64ec8c7d\","
        "\"image\":\"httpd\",\"name\":\"zarpit-httpd\"}},\"scope\":\"local\",\"time\":1524657459,"
        "\"timeNano\":1524657459340170830}"

        "{\"status\":\"exec_start: sh -c export http_proxy=http://10.115.27.54:3128 \u0026\u0026 "
        "export https_proxy=http://10.115.27.54:3128 \u0026\u0026 apt-get update \u0026\u0026 "
        "apt-get -y upgrade \u0026\u0026 echo Done "
        "Httpddocker\",\"id\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"from\":\"httpd\","
        "\"Type\":\"container\",\"Action\":\"exec_start: sh -c export "
        "http_proxy=http://10.115.27.54:3128 \u0026\u0026 export "
        "https_proxy=http://10.115.27.54:3128 \u0026\u0026 apt-get update \u0026\u0026 apt-get -y "
        "upgrade \u0026\u0026 echo Done "
        "Httpddocker\",\"Actor\":{\"ID\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"Attributes\":{"
        "\"execID\":\"fab2b57669cf8e6d6184f030123325c709ad053e5230e35152afad6f64ec8c7d\","
        "\"image\":\"httpd\",\"name\":\"zarpit-httpd\"}},\"scope\":\"local\",\"time\":1524657459,"
        "\"timeNano\":1524657459340989825}"

        "{\"status\":\"exec_die\",\"id\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"from\":\"httpd\","
        "\"Type\":\"container\",\"Action\":\"exec_die\",\"Actor\":{\"ID\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"Attributes\":{"
        "\"execID\":\"fab2b57669cf8e6d6184f030123325c709ad053e5230e35152afad6f64ec8c7d\","
        "\"exitCode\":\"0\",\"image\":\"httpd\",\"name\":\"zarpit-httpd\"}},\"scope\":\"local\","
        "\"time\":1524657475,\"timeNano\":1524657475947889181}";

    std::shared_ptr<MOCJSONStreamParserDelegate> mockProcessor =
        std::make_shared<MOCJSONStreamParserDelegate>();
    JSONByteStreamParser<2048> jsonParser(mockProcessor);

    EXPECT_CALL(*mockProcessor.get(), Process(_)).Times(3);
    jsonParser.ParseStream(byte_chunk((uint8_t*)json_string, strlen((char*)json_string)));
    EXPECT_TRUE(jsonParser.NumberOfJSONParsed() == 3);

    auto jsonProcessor = std::make_shared<JSONStreamProcessor>();
    JSONByteStreamParser<2048> parser(jsonProcessor);

    parser.ParseStream(byte_chunk((uint8_t*)json_string, strlen((char*)json_string)));
    EXPECT_TRUE(jsonProcessor->GetJobs().size() == 3);
}

TEST(TestSuiteCommonUtil, TestJSONStreamParserDockerEventQueuing)
{
    const char json_string[] =
        "{\"status\":\"exec_create: sh -c export http_proxy=http://10.115.27.54:3128 \u0026\u0026 "
        "export https_proxy=http://10.115.27.54:3128 \u0026\u0026 apt-get update \u0026\u0026 "
        "apt-get -y upgrade \u0026\u0026 echo Done "
        "Httpddocker\",\"id\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"from\":\"httpd\","
        "\"Type\":\"container\",\"Action\":\"exec_create: sh -c export "
        "http_proxy=http://10.115.27.54:3128 \u0026\u0026 export "
        "https_proxy=http://10.115.27.54:3128 \u0026\u0026 apt-get update \u0026\u0026 apt-get -y "
        "upgrade \u0026\u0026 echo Done "
        "Httpddocker\",\"Actor\":{\"ID\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"Attributes\":{"
        "\"execID\":\"fab2b57669cf8e6d6184f030123325c709ad053e5230e35152afad6f64ec8c7d\","
        "\"image\":\"httpd\",\"name\":\"zarpit-httpd\"}},\"scope\":\"local\",\"time\":1524657459,"
        "\"timeNano\":1524657459340170830}"

        "{\"status\":\"exec_start: sh -c export http_proxy=http://10.115.27.54:3128 \u0026\u0026 "
        "export https_proxy=http://10.115.27.54:3128 \u0026\u0026 apt-get update \u0026\u0026 "
        "apt-get -y upgrade \u0026\u0026 echo Done "
        "Httpddocker\",\"id\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"from\":\"httpd\","
        "\"Type\":\"container\",\"Action\":\"exec_start: sh -c export "
        "http_proxy=http://10.115.27.54:3128 \u0026\u0026 export "
        "https_proxy=http://10.115.27.54:3128 \u0026\u0026 apt-get update \u0026\u0026 apt-get -y "
        "upgrade \u0026\u0026 echo Done "
        "Httpddocker\",\"Actor\":{\"ID\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"Attributes\":{"
        "\"execID\":\"fab2b57669cf8e6d6184f030123325c709ad053e5230e35152afad6f64ec8c7d\","
        "\"image\":\"httpd\",\"name\":\"zarpit-httpd\"}},\"scope\":\"local\",\"time\":1524657459,"
        "\"timeNano\":1524657459340989825}"

        "{\"status\":\"exec_die\",\"id\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"from\":\"httpd\","
        "\"Type\":\"container\",\"Action\":\"exec_die\",\"Actor\":{\"ID\":"
        "\"7e4543dfa0e0ed5996eabd5db540935f5645f05aab31561f86e84ce28131e8c7\",\"Attributes\":{"
        "\"execID\":\"fab2b57669cf8e6d6184f030123325c709ad053e5230e35152afad6f64ec8c7d\","
        "\"exitCode\":\"0\",\"image\":\"httpd\",\"name\":\"zarpit-httpd\"}},\"scope\":\"local\","
        "\"time\":1524657475,\"timeNano\":1524657475947889181}";

    auto jsonProcessor = std::make_shared<JSONStreamProcessor>();
    {
        JSONByteStreamParser<2048> jsonParser(jsonProcessor);
        jsonParser.ParseStream(byte_chunk((uint8_t*)json_string, strlen((char*)json_string)));
    }
    EXPECT_TRUE(jsonProcessor->GetJobs().size() == 3);

    auto jobs = jsonProcessor->GetJobs();
    {
        jsonProcessor.reset();
    }
}

TEST(TestSuiteCommonUtil, TestJSONStreamParserOpenShiftEvent)
{
    const char json_string[] =
        "{"
        "	\"status\": \"pull\","
        "	\"id\": \"registry.access.redhat.com/openshift3/nodejs-010-rhel7:latest\","
        "	\"Type\": \"image\","
        "	\"Action\": \"pull\","
        "	\"Actor\": {"
        "		\"ID\": \"registry.access.redhat.com/openshift3/nodejs-010-rhel7:latest\","
        "		\"Attributes\": {"
        "			\"BZComponent\": \"openshift-sti-nodejs-docker\","
        "			\"Name\": \"openshift3/nodejs-010-rhel7\","
        "			\"Release\": \"47\","
        "			\"Version\": \"0.10\","
        "			\"architecture\": \"x86_64\","
        "			\"authoritative-source-url\": \"registry.access.redhat.com\","
        "			\"build-date\": \"2017-01-13T14:06:37.019370\","
        "			\"com.redhat.build-host\": \"ip-10-29-120-148.ec2.internal\","
        "			\"com.redhat.component\": \"openshift-sti-nodejs-docker\","
        "			\"com.redhat.deployments-dir\": \"/opt/app-root/src\","
        "			\"com.redhat.dev-mode\": \"DEV_MODE:false\","
        "			\"com.redhat.dev-mode.port\": \"DEBUG_PORT:5858\","
        "			\"description\": \"The Red Hat Enterprise Linux Base image is designed to be a fully supported "
        "foundation for your containerized applications.  This base image provides your operations and application "
        "teams with the packages, language runtimes and tools necessary to run, maintain, and troubleshoot all of your "
        "applications. This image is maintained by Red Hat and updated regularly. It is designed and engineered to be "
        "the base layer for all of your containerized applications, middleware and utilites. When used as the source "
        "for all of your containers, only one copy will ever be downloaded and cached in your production environment. "
        "Use this image just like you would a regular Red Hat Enterprise Linux distribution. Tools like yum, gzip, and "
        "bash are provided by default. For further information on how this image was built look at the "
        "/root/anacanda-ks.cfg file.\","
        "			\"distribution-scope\": \"public\","
        "			\"io.k8s.description\": \"Platform for building and running Node.js 0.10 applications\","
        "			\"io.k8s.display-name\": \"Node.js 0.10\","
        "			\"io.openshift.expose-services\": \"8080:http\","
        "			\"io.openshift.s2i.scripts-url\": \"image:///usr/libexec/s2i\","
        "			\"io.openshift.tags\": \"builder,nodejs,nodejs010\","
        "			\"io.s2i.scripts-url\": \"image:///usr/libexec/s2i\","
        "			\"name\": \"registry.access.redhat.com/openshift3/nodejs-010-rhel7\","
        "			\"release\": \"47\","
        "			\"summary\": \"Platform for building and running Node.js 0.10 applications\","
        "			\"vcs-ref\": \"b97dbc072c8c782b4cbb0864cbbb83ed71616c28\","
        "			\"vcs-type\": \"git\","
        "			\"vendor\": \"Red Hat, Inc.\","
        "			\"version\": \"0.10\""
        "		}"
        "	},"
        "	\"scope\": \"local\","
        "	\"time\": 1537445306,"
        "	\"timeNano\": 1537445306470551278"
        "}";

    std::shared_ptr<MOCJSONStreamParserDelegate> mockProcessor = std::make_shared<MOCJSONStreamParserDelegate>();
    JSONByteStreamParser<4048> jsonParser(mockProcessor);

    EXPECT_CALL(*mockProcessor.get(), Process(_)).Times(1);
    EXPECT_TRUE(jsonParser.ParseStream(byte_chunk((uint8_t*)json_string, strlen((char*)json_string))));
    EXPECT_TRUE(jsonParser.NumberOfJSONParsed() == 1);
}

TEST(TestSuiteCommonUtil, TestJSONStreamParserChunkedParsing)
{
    const char json_string_chunk_1[] =
        "{"
        "    \"type\": \"ADDED\", "
        "    \"object\": {"
        "        \"kind\": \"Pod\", "
        "        \"apiVersion\": \"v1\", "
        "        \"metadata\": {"
        "            \"name\": \"weave-net-5q5sc\", "
        "            \"generateName\": \"weave-net-\", "
        "            \"namespace\": \"kube-system\", "
        "            \"selfLink\": \"/api/v1/namespaces/kube-system/pods/weave-net-5q5sc\", "
        "            \"uid\": \"563ff40d-0a90-11e9-b4fd-0050568171d0\", "
        "            \"resourceVersion\": \"20430944\", "
        "            \"creationTimestamp\": \"2018-12-28T11:04:25Z\", "
        "            \"labels\": {"
        "                \"controller-revision-hash\": \"7d4c699864\", "
        "                \"name\": \"weave-net\", "
        "                \"pod-template-generation\": \"1\""
        "            }, "
        "            \"ownerReferences\": ["
        "                {"
        "                    \"apiVersion\": \"apps/v1\", "
        "                    \"kind\": \"DaemonSet\", "
        "                    \"name\": \"weave-net\", "
        "                    \"uid\": \"563d56cd-0a90-11e9-b4fd-0050568171d0\", "
        "                    \"controller\": true, "
        "                    \"blockOwnerDeletion\": true"
        "                }"
        "            ]"
        "        }, "
        "        \"spec\": {"
        "            \"volumes\": [ ], "
        "            \"containers\": ["
        "                {"
        "                    \"name\": \"weave\", "
        "                    \"image\": \"docker.io/weaveworks/weave-kube:2.5.0\", "
        "                    \"command\": [ ], "
        "                    \"env\": [ ], "
        "                    \"resources\": { }, "
        "                    \"volumeMounts\": [ ], "
        "                    \"readinessProbe\": { }, "
        "                    \"terminationMessagePath\": \"/dev/termination-log\", "
        "                    \"terminationMessagePolicy\": \"File\", "
        "                    \"imagePullPolicy\": \"IfNotPresent\", "
        "                    \"security";

        
    const char json_string_chunk_2[] =
        "Context\": { }"
        "                }, "
        "                {"
        "                    \"name\": \"weave-npc\", "
        "                    \"image\": \"docker.io/weaveworks/weave-npc:2.5.0\", "
        "                    \"env\": ["
        "                        {"
        "                            \"name\": \"HOSTNAME\", "
        "                            \"valueFrom\": {"
        "                                \"fieldRef\": {"
        "                                    \"apiVersion\": \"v1\", "
        "                                    \"fieldPath\": \"spec.nodeName\""
        "                                }"
        "                            }"
        "                        }"
        "                    ], "
        "                    \"resources\": {"
        "                        \"requests\": {"
        "                            \"cpu\": \"10m\""
        "                        }"
        "                    }, "
        "                    \"volumeMounts\": ["
        "                        {"
        "                            \"name\": \"xtables-lock\", "
        "                            \"mountPath\": \"/run/xtables.lock\""
        "                        }, "
        "                        {"
        "                            \"name\": \"weave-net-token-n26hz\", "
        "                            \"readOnly\": true, "
        "                            \"mountPath\": \"/var/run/secrets/kubernetes.io/serviceaccount\""
        "                        }"
        "                    ], "
        "                    \"terminationMessagePath\": \"/dev/termination-log\", "
        "                    \"terminationMessagePolicy\": \"File\", "
        "                    \"imagePullPolicy\": \"IfNotPresent\", "
        "                    \"securityContext\": {"
        "                        \"privileged\": true, "
        "                        \"procMount\": \"Default\""
        "                    }"
        "                }"
        "            ], "
        "            \"restartPolicy\": \"Always\", "
        "            \"terminationGracePeriodSeconds\": 30, "
        "            \"dnsPolicy\": \"ClusterFirst\", "
        "            \"serviceAccountName\": \"weave-net\", "
        "            \"serviceAccount\": \"weave-net\", "
        "            \"nodeName\": \"kubemaster3\", "
        "            \"hostNetwork\": true, "
        "            \"hostPID\": true, "
        "            \"securityContext\": {"
        "                \"seLinuxOptions\": { }"
        "            }, "
        "            \"affinity\": {"
        "                \"nodeAffinity\": {"
        "                    \"requiredDuringSchedulingIgnoredDuringExecution\": {"
        "                        \"nodeSelectorTerms\": ["
        "                            {"
        "                                \"matchFields\": ["
        "                                    {"
        "                                        \"key\": \"metadata.name\", "
        "                                        \"operator\": \"In\", "
        "                                        \"values\": ["
        "                                            \"kubemaster3\""
        "                                        ]"
        "                                    }"
        "                                ]"
        "                            }"
        "                        ]"
        "                    }"
        "                }"
        "            }, "
        "            \"schedulerName\": \"default-scheduler\", "
        "            \"tolerations\": ["
        "                {"
        "                    \"operator\": \"Exists\", "
        "                    \"effect\": \"NoSchedule\""
        "                }, "
        "                {"
        "                    \"key\": \"node.kubernetes.io/not-ready\", "
        "                    \"operator\": \"Exists\", "
        "                    \"effect\": \"NoExecute\""
        "                }, "
        "                {"
        "                    \"key\": \"node.kubernetes.io/unreachable\", "
        "                    \"operator\": \"Exists\", "
        "                    \"effect\": \"NoExecute\""
        "                }, "
        "                {"
        "                    \"key\": \"node.kubernetes.io/disk-pressure\", "
        "                    \"operator\": \"Exists\", "
        "                    \"effect\": \"NoSchedule\""
        "                }, "
        "                {"
        "                    \"key\": \"node.kubernetes.io/memory-pressure\", "
        "                    \"operator\": \"Exists\", "
        "                    \"effect\": \"NoSchedule\""
        "                }, "
        "                {"
        "                    \"key\": \"node.kubernetes.io/unschedulable\", "
        "                    \"operator\": \"Exists\", "
        "                    \"effect\": \"NoSchedule\""
        "                }, "
        "                {"
        "                    \"key\": \"node.kubernetes.io/network-unavailable\", "
        "                    \"operator\": \"Exists\", "
        "                    \"effect\": \"NoSchedule\""
        "                }"
        "            ], "
        "            \"priority\": 0, "
        "            \"enableServiceLinks\": true"
        "        }, "
        "        \"status\": { }"
        "    }"
        "}";

    {
        auto jsonProcessor = std::make_shared<JSONStreamProcessor>();
        JSONByteStreamParser<100 * 1024> jsonParser(jsonProcessor);

        jsonParser.ParseStream(byte_chunk((uint8_t*)json_string_chunk_1, strlen((char*)json_string_chunk_1)));
        jsonParser.ParseStream(byte_chunk((uint8_t*)json_string_chunk_2, strlen((char*)json_string_chunk_2)));
        EXPECT_TRUE(jsonProcessor->GetJobs().size() == 1);
        // temp work, uncomment when testing.
        // for (auto job : jsonProcessor->GetJobs()) std::cout << kld::foundation::GetPrettyFormattedStringFromJSONDoc(*job);
    }

    {
        std::shared_ptr<MOCJSONStreamParserDelegate> mockProcessor = std::make_shared<MOCJSONStreamParserDelegate>();
        JSONByteStreamParser<100 * 1024> jsonParser(mockProcessor);

        EXPECT_CALL(*mockProcessor.get(), Process(_)).Times(0);
        EXPECT_TRUE(
            jsonParser.ParseStream(byte_chunk((uint8_t*)json_string_chunk_1, strlen((char*)json_string_chunk_1))));
        EXPECT_CALL(*mockProcessor.get(), Process(_)).Times(1);
        EXPECT_TRUE(
            jsonParser.ParseStream(byte_chunk((uint8_t*)json_string_chunk_2, strlen((char*)json_string_chunk_2))));
        EXPECT_TRUE(jsonParser.NumberOfJSONParsed() == 1);
    }
}