#ifdef USE_GTEST_HEADER_ONLY
#include "src/gtest-all.cc"
#undef USE_GTEST_HEADER_ONLY
#endif

#ifdef USE_GTEST_MAIN
#include "src/gtest_main.cc"
#undef USE_GTEST_MAIN
#endif

#include <string.h>
#include "foundation/byte_buffer.h"
#include "gtest/gtest.h"

using namespace std;
using namespace kld;
TEST(TestSuiteCommonUtil, TestByteBufferInsertBelowCapacity)
{
    foundation::byte_buffer<10> mybuff;
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_EQ(mybuff.capacity(), 10);

    const unsigned char hi[] = "Hello World, This Is Different World Dear Friends";
    EXPECT_EQ(mybuff.insert_back(hi, 5), 0);
    EXPECT_STREQ(mybuff.c_str(), "Hello");
    EXPECT_EQ(mybuff.size(), 5);
    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_FALSE(mybuff.is_full());
}

TEST(TestSuiteCommonUtil, TestByteBufferInsertAboveCapacity)
{
    foundation::byte_buffer<11> mybuff;
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_EQ(mybuff.capacity(), 11);

    const unsigned char hi[] = "Hello World, This Is Different World Dear Friends";
    EXPECT_EQ(mybuff.insert_back(hi, 25), 14);
    EXPECT_STREQ(mybuff.c_str(), "Hello World");
    EXPECT_EQ(mybuff.size(), 11);
    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_TRUE(mybuff.is_full());
}

TEST(TestSuiteCommonUtil, TestByteBufferMultipleInsertWithinCapacity)
{
    foundation::byte_buffer<11> mybuff;
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_EQ(mybuff.capacity(), 11);

    const unsigned char hi[] = "Hello World, This Is Different World Dear Friends";
    EXPECT_EQ(mybuff.insert_back(hi, 5), 0);
    EXPECT_STREQ(mybuff.c_str(), "Hello");
    EXPECT_EQ(mybuff.size(), 5);
    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_FALSE(mybuff.is_full());

    EXPECT_EQ(mybuff.insert_back(hi + 5, 2), 0);
    EXPECT_STREQ(mybuff.c_str(), "Hello W");
    EXPECT_EQ(mybuff.size(), 7);

    EXPECT_EQ(mybuff.insert_back(hi + 7, 2), 0);
    EXPECT_STREQ(mybuff.c_str(), "Hello Wor");
    EXPECT_EQ(mybuff.size(), 9);
    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_FALSE(mybuff.is_full());

    EXPECT_EQ(mybuff.insert_back(hi + 9, 2), 0);
    EXPECT_STREQ(mybuff.c_str(), "Hello World");
    EXPECT_EQ(mybuff.size(), 11);

    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_TRUE(mybuff.is_full());
}

TEST(TestSuiteCommonUtil, TestByteBufferInsertAfterFull)
{
    foundation::byte_buffer<11> mybuff;
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_EQ(mybuff.capacity(), 11);

    const unsigned char hi[] = "Hello World, This Is Different World Dear Friends";
    EXPECT_EQ(mybuff.insert_back(hi, 25), 14);
    EXPECT_STREQ(mybuff.c_str(), "Hello World");
    EXPECT_EQ(mybuff.size(), 11);
    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_TRUE(mybuff.is_full());

    EXPECT_EQ(mybuff.insert_back(hi + 11, 5), 5);
    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_TRUE(mybuff.is_full());
    EXPECT_STREQ(mybuff.c_str(), "Hello World");
    EXPECT_EQ(mybuff.size(), 11);
}

TEST(TestSuiteCommonUtil, TestByteBufferRemove)
{
    foundation::byte_buffer<10> mybuff;
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_EQ(mybuff.capacity(), 10);

    const unsigned char hi[] = "Hello World, This Is Different World Dear Friends";
    EXPECT_EQ(mybuff.insert_back(hi, 5), 0);
    EXPECT_STREQ(mybuff.c_str(), "Hello");
    EXPECT_EQ(mybuff.size(), 5);
    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_FALSE(mybuff.is_full());

    mybuff.remove_front(2);
    EXPECT_STREQ(mybuff.c_str(), "llo");
    EXPECT_EQ(mybuff.size(), 3);

    mybuff.remove_front(3);
    EXPECT_STREQ(mybuff.c_str(), "");
    EXPECT_EQ(mybuff.size(), 0);
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_FALSE(mybuff.is_full());
}

TEST(TestSuiteCommonUtil, TestByteBufferRemoveChar)
{
    foundation::byte_buffer<10> mybuff;
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_EQ(mybuff.capacity(), 10);

    const unsigned char hi[] = "Hello World, This Is Different World Dear Friends";
    EXPECT_EQ(mybuff.insert_back(hi, 5), 0);
    EXPECT_STREQ(mybuff.c_str(), "Hello");
    EXPECT_EQ(mybuff.size(), 5);
    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_FALSE(mybuff.is_full());

    EXPECT_TRUE(mybuff.remove_front_till('H'));
    EXPECT_STREQ(mybuff.c_str(), "ello");
    EXPECT_EQ(mybuff.size(), 4);

    EXPECT_TRUE(mybuff.remove_front_till('o'));
    EXPECT_STREQ(mybuff.c_str(), "");
    EXPECT_EQ(mybuff.size(), 0);
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_FALSE(mybuff.is_full());
}

TEST(TestSuiteCommonUtil, TestByteBufferCorrectsPositionToFit)
{
    foundation::byte_buffer<10> mybuff;
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_EQ(mybuff.capacity(), 10);

    const unsigned char hi[] = "Hello World, This Is Different World Dear Friends";
    EXPECT_EQ(mybuff.insert_back(hi, 5), 0);
    EXPECT_STREQ(mybuff.c_str(), "Hello");
    EXPECT_EQ(mybuff.size(), 5);
    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_FALSE(mybuff.is_full());

    mybuff.remove_front(4);
    EXPECT_STREQ(mybuff.c_str(), "o");
    EXPECT_EQ(mybuff.size(), 1);

    EXPECT_EQ(mybuff.insert_back(hi + 5, 6), 0);
    EXPECT_STREQ(mybuff.c_str(), "o World");
    EXPECT_EQ(mybuff.size(), 7);

    mybuff.remove_front(3);
    EXPECT_EQ(mybuff.size(), 4);
    EXPECT_STREQ(mybuff.c_str(), "orld");

    // Test if buffer postions are corrected to fit and returns remaining overfolow peroperly
    EXPECT_EQ(mybuff.insert_back(hi, 10), 4);
    EXPECT_STREQ(mybuff.c_str(), "orldHello ");
    EXPECT_EQ(mybuff.size(), 10);
}

TEST(TestSuiteCommonUtil, TestByteBufferBoundryInsertRemove)
{
    foundation::byte_buffer<11> mybuff;
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_EQ(mybuff.capacity(), 11);

    const unsigned char hi[] = "Hello World, This Is Different World Dear Friends";
    EXPECT_EQ(mybuff.insert_back(hi, 11), 0);
    EXPECT_STREQ(mybuff.c_str(), "Hello World");
    EXPECT_EQ(mybuff.size(), 11);
    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_TRUE(mybuff.is_full());

    mybuff.remove_front(10);
    EXPECT_STREQ(mybuff.c_str(), "d");
    EXPECT_EQ(mybuff.size(), 1);

    EXPECT_EQ(mybuff.insert_back(hi + 11, 1), 0);
    EXPECT_STREQ(mybuff.c_str(), "d,");
    EXPECT_EQ(mybuff.size(), 2);

    EXPECT_EQ(mybuff.insert_back(hi + 12, 1), 0);
    EXPECT_STREQ(mybuff.c_str(), "d, ");
    EXPECT_EQ(mybuff.size(), 3);
}

TEST(TestSuiteCommonUtil, TestByteBufferClearAfterOverflow)
{
    foundation::byte_buffer<12> mybuff;
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_EQ(mybuff.capacity(), 12);

    const unsigned char hi[] = "Hello World, This Is Different World Dear Friends";

    foundation::byte_chunk my_data(hi, strlen((char *)hi));

    foundation::byte_chunk overflow = mybuff.insert_back(my_data);
    EXPECT_STREQ((char *)overflow.chunk, " This Is Different World Dear Friends");
    EXPECT_EQ(overflow.size, 37);

    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_TRUE(mybuff.is_full());

    mybuff.clear();
    EXPECT_TRUE(mybuff.is_empty());
    EXPECT_FALSE(mybuff.is_full());
}

TEST(TestSuiteCommonUtil, TestByteBufferOverFlow)
{
    const unsigned char json_string[] =
        "{ \"hello1\" : \"world1\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": "
        "3.1416, \"a\":[1, 2, 3, 4] } ";

    foundation::byte_buffer<50> mybuff;

    foundation::byte_chunk my_data(json_string, strlen((char *)json_string));

    foundation::byte_chunk overflow = my_data;
    while (overflow.chunk != nullptr) {
        overflow = mybuff.insert_back(overflow);
        mybuff.remove_front(overflow.size);
    }

    EXPECT_FALSE(mybuff.is_empty());
    EXPECT_TRUE(mybuff.is_full());
}
