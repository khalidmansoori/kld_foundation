// gtest

#ifdef USE_GTEST_HEADER_ONLY
#include "src/gtest-all.cc"
#undef USE_GTEST_HEADER_ONLY
#endif

#ifdef USE_GTEST_MAIN
#include "src/gtest_main.cc"
#undef USE_GTEST_MAIN
#endif

#include "gtest/gtest.h"

#include "foundation/observer.h"
#include <iostream>
#include <string>

using namespace std;
using namespace std::placeholders;
using namespace kld::foundation;

class MyDataObserver : public observer_t<int> {
public:
  virtual void on_update(int i) override {
    std::cout << __PRETTY_FUNCTION__ << ":" << i << std::endl;
  }
};

class Foo {
public:
  int data() {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    return 1;
  };
};

class MyObjectObserver : public observer_t<Foo> {
public:
  virtual void on_update(Foo o) override {
    std::cout << __PRETTY_FUNCTION__ << ":" << o.data() << std::endl;
  }
};

class MyObjectObserverSecond : public observer_t<Foo> {
public:
  virtual void on_update(Foo o) override {
    std::cout << __PRETTY_FUNCTION__ << ":" << o.data() << std::endl;
  }
};

class SystemObserver {
public:
  virtual void onSuccess(std::string s) {
    std::cout << __PRETTY_FUNCTION__ << ":" << s << std::endl;
  }
  virtual void onFailure() { std::cout << __PRETTY_FUNCTION__ << std::endl; }
};

class SystemEventProducer;

class SystemLogObserver : public observer_t<std::string> {
public:
  virtual void onWriteSuccessLog(std::string s) {
    std::cout << __PRETTY_FUNCTION__ << ":" << s << std::endl;
  }
  virtual void onWriteFailure() {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
  }
  virtual void onWriteSender(std::shared_ptr<SystemEventProducer> producer) {
    std::cout << __PRETTY_FUNCTION__ << std::endl;
  }

  virtual void on_update(std::string s) override {
    std::cout << __PRETTY_FUNCTION__ << ":" << s << std::endl;
  }
};

class SystemEventProducer
    : public observable_t<SystemObserver>,
      public observable_t<SystemLogObserver>,
      public observable_t<MyDataObserver>,
      public enable_shared_from_this<SystemEventProducer> {
public:
  SystemEventProducer(std::weak_ptr<SystemObserver> systemObserver)
      : systemObserver_(systemObserver) {}

  void addLogObserver(std::weak_ptr<SystemLogObserver> systemLogObserver) {
    systemLogObserver_ = systemLogObserver;
    observable_t<SystemLogObserver>::add_observer(systemLogObserver_);
  }

  void addIntObserver(std::weak_ptr<MyDataObserver> intListetener) {

    observable_t<MyDataObserver>::add_observer(intListetener);
  }

  void init() {
    observable_t<SystemObserver>::add_observer(systemObserver_);
    observable_t<SystemLogObserver>::add_observer(systemLogObserver_);
  }

  void produce() {
    observable_t<SystemObserver>::notify_observers(&SystemObserver::onSuccess, "hello");
    observable_t<SystemObserver>::notify_observers(&SystemObserver::onFailure);

    observable_t<SystemLogObserver>::notify_observers(&SystemLogObserver::onWriteSuccessLog,
                                          "hello");
    observable_t<SystemLogObserver>::notify_observers(&SystemLogObserver::onWriteFailure);
    observable_t<SystemLogObserver>::update_observers("hello");
    observable_t<SystemLogObserver>::notify_observers(&SystemLogObserver::onWriteSender,
                                          shared_from_this());
    observable_t<MyDataObserver>::update_observers(99999);
  }

  void print() { std::cout << __PRETTY_FUNCTION__ << std::endl; }

private:
  std::weak_ptr<SystemObserver> systemObserver_;
  std::weak_ptr<SystemLogObserver> systemLogObserver_;
};

class SpecialSystemObserver : public SystemObserver {
public:
  void onSuccess(std::string s) override {
    std::cout << __PRETTY_FUNCTION__ << ":" << s << std::endl;
  }
  void onFailure() override { std::cout << __PRETTY_FUNCTION__ << std::endl; }
};

TEST(TestSuiteFoundation, TestSimpleObserver) {
 auto systemObserver = make_shared<SystemObserver>();
  observable_t<SystemObserver> o;
  o.add_observer(systemObserver);

  // Check it should notify.
  o.notify_observers(&SystemObserver::onSuccess, "OK");
  o.notify_observers(&SystemObserver::onFailure);

  o.remove_observer(systemObserver);

  //Should not notify
  o.notify_observers(&SystemObserver::onSuccess, "OK");
  o.notify_observers(&SystemObserver::onFailure);

  // Should not notify
   o.add_observer(systemObserver);
   systemObserver.reset();
   o.notify_observers(&SystemObserver::onSuccess, "OK");
  o.notify_observers(&SystemObserver::onFailure);
}

TEST(TestSuiteFoundation, TestPolymorphicClassObserver) {

  auto systemObserver = make_shared<SystemObserver>();
  
  auto producer = make_shared<SystemEventProducer>(systemObserver);
  producer->init();
  auto logObserver = make_shared<SystemLogObserver>();
  producer->addLogObserver(logObserver);
  producer->produce();

  auto specialObserver = make_shared<SpecialSystemObserver>();

  auto specialProducer = make_shared<SystemEventProducer>(specialObserver);

  auto integerObserver = make_shared<MyDataObserver>();
  specialProducer->addIntObserver(integerObserver);

  specialProducer->init();
  specialObserver.reset();
  specialProducer->produce();
  specialProducer->produce();

  specialProducer->produce();
  specialProducer->observable_t<MyDataObserver>::remove_observer(integerObserver);
  specialProducer->produce();

}

TEST(TestSuiteFoundation, TestStandardObserver) {
  observable_t<observer_t<Foo>> myobj;
  auto obj1 = make_shared<MyObjectObserver>();
  auto obj2 = make_shared<MyObjectObserver>();
  auto obj3 = make_shared<MyObjectObserverSecond>();
  myobj.add_observer(obj1);
  myobj.add_observer(obj2);
  myobj.add_observer(obj3);
  myobj.add_observer(obj1);
  myobj.add_observer(obj1);
  myobj.add_observer(obj2);
  myobj.update_observers(Foo());

  observable_t<Foo> myfunc;
  auto foo1 = make_shared<Foo>();
  auto foo2 = make_shared<Foo>();
  auto foo3 = make_shared<Foo>();
  myfunc.add_observer(foo1);
  myfunc.add_observer(foo2);
  myfunc.add_observer(foo3);
  myfunc.notify_observers(&Foo::data);
}
